﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TomaszWojciechowskiWzorzecProjektowyMost.Logic;

namespace TomaszWojciechowskiWzorzecProjektowyMost
{
    /// <summary>
    /// Okno główne
    /// </summary>
    public partial class FormMain : Form
    {
        /// <summary>
        /// Obiekt działanie
        /// </summary>
        private IAction action;

        /// <summary>
        /// Konstruktor
        /// </summary>
        public FormMain()
        {
            InitializeComponent();
            comboBoxAction.SelectedIndex = 0;
        }

        /// <summary>
        /// Zmiana pozycji menu rozwijanego powoduje zmianę zapisanego działania
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBoxAction_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBoxAction.SelectedIndex)
            {
                case 0:
                    action = new Add();
                    break;
                case 1:
                    action = new Substract();
                    break;
                case 2:
                    action = new Multiple();
                    break;
                case 3:
                    action = new Division();
                    break;
                case 4:
                    action = new Power();
                    break;
                case 5:
                    action = new Modulo();
                    break;
            }
        }

        /// <summary>
        /// Wciśnięcie przycisku powoduje sprawdzenie poprawności danych i ewentualnie obliczenie wyniku
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCalculate_Click(object sender, EventArgs e)
        {
            float x, y;
            if (!float.TryParse(textBoxX.Text, out x) || !float.TryParse(textBoxY.Text, out y))
            {
                MessageBox.Show("Błędne wartosći w polach liczbowych", "Błąd!");
                return;
            }
            textBoxOut.Text = action.Calculate(x, y);
        }
    }
}
