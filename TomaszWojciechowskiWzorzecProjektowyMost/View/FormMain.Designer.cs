﻿namespace TomaszWojciechowskiWzorzecProjektowyMost
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxAction = new System.Windows.Forms.ComboBox();
            this.labelX = new System.Windows.Forms.Label();
            this.labelY = new System.Windows.Forms.Label();
            this.labelAction = new System.Windows.Forms.Label();
            this.labelOut = new System.Windows.Forms.Label();
            this.buttonCalculate = new System.Windows.Forms.Button();
            this.textBoxY = new System.Windows.Forms.TextBox();
            this.textBoxOut = new System.Windows.Forms.TextBox();
            this.textBoxX = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // comboBoxAction
            // 
            this.comboBoxAction.AutoCompleteCustomSource.AddRange(new string[] {
            "x + y",
            "x - y",
            "x * y",
            "x / y",
            "x ^ y",
            "x % y"});
            this.comboBoxAction.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxAction.FormattingEnabled = true;
            this.comboBoxAction.Items.AddRange(new object[] {
            "x + y",
            "x - y",
            "x * y",
            "x / y",
            "x ^ y",
            "x % y"});
            this.comboBoxAction.Location = new System.Drawing.Point(105, 78);
            this.comboBoxAction.Name = "comboBoxAction";
            this.comboBoxAction.Size = new System.Drawing.Size(154, 21);
            this.comboBoxAction.TabIndex = 0;
            this.comboBoxAction.SelectedIndexChanged += new System.EventHandler(this.comboBoxAction_SelectedIndexChanged);
            // 
            // labelX
            // 
            this.labelX.AutoSize = true;
            this.labelX.Location = new System.Drawing.Point(30, 29);
            this.labelX.Name = "labelX";
            this.labelX.Size = new System.Drawing.Size(21, 13);
            this.labelX.TabIndex = 1;
            this.labelX.Text = "x =";
            // 
            // labelY
            // 
            this.labelY.AutoSize = true;
            this.labelY.Location = new System.Drawing.Point(30, 55);
            this.labelY.Name = "labelY";
            this.labelY.Size = new System.Drawing.Size(21, 13);
            this.labelY.TabIndex = 2;
            this.labelY.Text = "y =";
            // 
            // labelAction
            // 
            this.labelAction.AutoSize = true;
            this.labelAction.Location = new System.Drawing.Point(30, 81);
            this.labelAction.Name = "labelAction";
            this.labelAction.Size = new System.Drawing.Size(52, 13);
            this.labelAction.TabIndex = 3;
            this.labelAction.Text = "Działanie";
            // 
            // labelOut
            // 
            this.labelOut.AutoSize = true;
            this.labelOut.Location = new System.Drawing.Point(30, 137);
            this.labelOut.Name = "labelOut";
            this.labelOut.Size = new System.Drawing.Size(37, 13);
            this.labelOut.TabIndex = 4;
            this.labelOut.Text = "Wynik";
            // 
            // buttonCalculate
            // 
            this.buttonCalculate.Location = new System.Drawing.Point(105, 105);
            this.buttonCalculate.Name = "buttonCalculate";
            this.buttonCalculate.Size = new System.Drawing.Size(154, 23);
            this.buttonCalculate.TabIndex = 5;
            this.buttonCalculate.Text = "Oblicz";
            this.buttonCalculate.UseVisualStyleBackColor = true;
            this.buttonCalculate.Click += new System.EventHandler(this.buttonCalculate_Click);
            // 
            // textBoxY
            // 
            this.textBoxY.Location = new System.Drawing.Point(105, 52);
            this.textBoxY.Name = "textBoxY";
            this.textBoxY.Size = new System.Drawing.Size(154, 20);
            this.textBoxY.TabIndex = 6;
            // 
            // textBoxOut
            // 
            this.textBoxOut.Location = new System.Drawing.Point(105, 134);
            this.textBoxOut.Name = "textBoxOut";
            this.textBoxOut.ReadOnly = true;
            this.textBoxOut.Size = new System.Drawing.Size(154, 20);
            this.textBoxOut.TabIndex = 7;
            // 
            // textBoxX
            // 
            this.textBoxX.Location = new System.Drawing.Point(105, 26);
            this.textBoxX.Name = "textBoxX";
            this.textBoxX.Size = new System.Drawing.Size(154, 20);
            this.textBoxX.TabIndex = 8;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(289, 181);
            this.Controls.Add(this.textBoxX);
            this.Controls.Add(this.textBoxOut);
            this.Controls.Add(this.textBoxY);
            this.Controls.Add(this.buttonCalculate);
            this.Controls.Add(this.labelOut);
            this.Controls.Add(this.labelAction);
            this.Controls.Add(this.labelY);
            this.Controls.Add(this.labelX);
            this.Controls.Add(this.comboBoxAction);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormMain";
            this.Text = "Program";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxAction;
        private System.Windows.Forms.Label labelX;
        private System.Windows.Forms.Label labelY;
        private System.Windows.Forms.Label labelAction;
        private System.Windows.Forms.Label labelOut;
        private System.Windows.Forms.Button buttonCalculate;
        private System.Windows.Forms.TextBox textBoxY;
        private System.Windows.Forms.TextBox textBoxOut;
        private System.Windows.Forms.TextBox textBoxX;
    }
}

