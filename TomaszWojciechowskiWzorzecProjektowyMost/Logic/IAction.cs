﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TomaszWojciechowskiWzorzecProjektowyMost.Logic
{
    /// <summary>
    /// Interfejs definiujący dowolne działanie
    /// </summary>
    public interface IAction
    {
        /// <summary>
        /// Działanie na dwóch liczbach
        /// </summary>
        /// <param name="a">Pierwsza liczba</param>
        /// <param name="b">Druga liczba</param>
        /// <returns>Wynik w postaci string</returns>
        string Calculate(float a, float b);
    }
}
