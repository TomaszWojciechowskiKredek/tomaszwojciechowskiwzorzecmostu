﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TomaszWojciechowskiWzorzecProjektowyMost.Logic
{
    /// <summary>
    /// Klasa implementująca modulo
    /// </summary>
    class Modulo : IAction
    {
        /// <summary>
        /// Metoda implementujaca modulo
        /// </summary>
        /// <param name="a">Pierwsza liczba</param>
        /// <param name="b">Druga liczba</param>
        /// <returns>Wynik</returns>
        public string Calculate(float a, float b)
        {
            return (a%b).ToString();
        }
    }
}
