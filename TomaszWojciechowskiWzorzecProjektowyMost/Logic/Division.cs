﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TomaszWojciechowskiWzorzecProjektowyMost.Logic
{
    /// <summary>
    /// Klasa implementująca dzielenie
    /// </summary>
    class Division : IAction
    {
        /// <summary>
        /// Metoda implementujaca dzielenie
        /// </summary>
        /// <param name="a">Pierwsza liczba</param>
        /// <param name="b">Druga liczba</param>
        /// <returns>Komunikat błędu gdy b = 0, w przeciwnym razie wynik</returns>
        public string Calculate(float a, float b)
        {
            if (b == 0)
                return "y != 0";
            return (a / b).ToString();
        }
    }
}
